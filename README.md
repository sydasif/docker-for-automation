# Docker for Network Engineers

[Docker for Network Engineers — Docker Intro](https://medium.com/@sydasif78/docker-for-network-automation-2d5a2df05453)

[Docker for Network Engineers — Docker Networking](https://medium.com/@sydasif78/docker-for-network-engineers-9e7268bd6e63)

[Docker for Network Engineers — Docker App](https://medium.com/@sydasif78/docker-for-network-engineers-03-c1e205f0ddd9)

[Docker for Network Engineers — Docker Lab](https://medium.com/@sydasif78/docker-for-network-engineers-04-a97664342953)

---
---

[Docker Networking Basics — Default Bridge Network](https://medium.com/@sydasif78/docker-networking-basics-network-modes-a714ef8851bf)

[Docker Networking Basics — User-defined Bridge Network](https://medium.com/@sydasif78/docker-networking-basics-user-defined-bridge-6eea891ee339)

[Docker Networking Basics — Host Network](https://medium.com/@sydasif78/docker-networking-basics-host-network-b424b26ffa58)

[Docker Networking Advance — Mac VLAN Network](https://medium.com/@sydasif78/docker-networking-advance-mac-vlan-network-b60ffab218cd)

[Docker Networking Advanced — IP VLAN Layer 2 Network](https://medium.com/@sydasif78/docker-networking-advanced-ip-vlan-layer-2-network-39a3613e81b8)

[Docker Networking Advanced — IP VLAN Layer 3 Network](https://medium.com/@sydasif78/docker-networking-advanced-ip-vlan-layer-3-network-ee27e996b418)
