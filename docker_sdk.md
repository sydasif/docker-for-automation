# A Beginner's Guide to Docker Python SDK

## Introduction

Welcome to our beginner-friendly guide on using the Docker SDK for Python. In this guide, we'll show you how to work with Docker containers using Python. You don't need any special environment setup or prior experience to get started.

## Why Docker and Python?

Docker is a powerful tool that makes it easy to run applications consistently across different environments. As a Python developer, you can simplify container management using the Docker SDK for Python. It lets you control Docker from your Python scripts, automating tasks and making your life easier.

## Step 1: Installing the Docker SDK for Python

Before we begin, you need to install the Docker SDK for Python. It's a Python package that allows you to work with Docker in Python. To install it, open your terminal or command prompt and run:

```bash
pip install docker
```

This will download and install the Docker SDK for Python.

## Step 2: Importing the Docker SDK

In your Python script or Jupyter Notebook, you need to import the Docker SDK to access its features. It's as simple as adding this line to the top of your Python code:

```python
import docker
```

This line of code makes the Docker SDK available in your Python script.

## Step 3: Creating a Docker Client

To interact with Docker, you need a client that can communicate with the Docker daemon on your system. It's easy to create a client instance in Python:

```python
client = docker.from_env()
```

This client will allow you to manage Docker containers and images.

## Step 4: Listing Running Containers

One common task with Docker is listing running containers. With the Docker SDK for Python, you can do this easily. Here's how to list running containers:

```python
containers = client.containers.list()
for container in containers:
    print(f"Container ID: {container.id}, Name: {container.name}")
```

This code snippet fetches and displays information about your running containers.

## Step 5: Pulling a Docker Image

To use a Docker image, you need to pull it from a Docker registry. Here's how you can pull an image, such as the "ubuntu" image with the "latest" tag:

```python
image_name = "ubuntu"
image_tag = "latest"
client.images.pull(image_name, tag=image_tag)
```

This code ensures that you have the required image available for use.

## Step 6: Running a Docker Container

Running a Docker container is a fundamental operation. Let's say you want to run an Nginx container in detached mode (in the background). You can do so with this code:

```python
image_name = "nginx"
image_tag = "latest"
container = client.containers.run(f"{image_name}:{image_tag}", detach=True)
```

The `container` object represents your running Nginx container.

## Step 7: Stopping a Docker Container

When you're done with a container, it's crucial to know how to stop it. Using the `stop()` method, you can halt a running container by its ID or name:

```python
container_id_or_name = "your_container_id_or_name"
container = client.containers.get(container_id_or_name)
container.stop()
```

This code ensures that your containers are correctly managed.

## Step 8: Additional Docker Operations

The Docker SDK for Python provides a wide range of functions for handling various Docker-related tasks. Here are a few more common operations you can explore:

### Removing a Docker Container

You can remove a stopped container using the `remove()` method:

```python
container_id_or_name = "your_container_id_or_name"
container = client.containers.get(container_id_or_name)
container.remove()
```

### Building a Docker Image

You can build a Docker image from a Dockerfile using the `images.build()` method:

```python
client.images.build(path="/path/to/dockerfile", tag="my-custom-image:latest")
```

### Pushing a Docker Image

To push a Docker image to a registry, use the `images.push()` method:

```python
image_name = "my-custom-image"
image_tag = "latest"
client.images.push(f"{image_name}:{image_tag}")
```

## Conclusion

Congratulations! You've completed our beginner's guide to the Docker SDK for Python. You've learned the essentials, from installing the SDK to performing basic Docker tasks. This is just the beginning of your Docker journey, and there's much more to explore. Docker is a valuable tool for automating, managing, and simplifying the deployment of applications, and with Python, the possibilities are endless.

[Happy Dockerizing! 🐳](https://docker-py.readthedocs.io/en/stable/index.html)
