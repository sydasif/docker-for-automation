# Import the Docker SDK for Python
import docker

# Create a Docker client using from_env()
client = docker.from_env()

# Get a reference to an existing container named 'netmiko'
container = client.containers.get('netmiko')

# Start the 'netmiko' container
container.start()

# Execute the command 'python app.py' inside the container
output = container.exec_run('python app.py')

# Print the output of the command executed inside the container (decoded from bytes to utf-8)
print(output[1].decode('utf-8'))

# Stop the container
container.stop()
