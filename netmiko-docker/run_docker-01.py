# Import the Docker SDK for Python
import docker

# Create a Docker client using from_env()
client = docker.from_env()

# Run a Docker container with specified image and command
# In this example, we run a container from the "automation:latest" image
# and execute the command "python app.py" inside the container
# auto_remove=True automatically removes the container when it stops
output = client.containers.run("automation:latest", "python app.py", auto_remove=True)

# Print the output from the container (decode it from bytes to utf-8)
print(output.decode('utf-8'))
