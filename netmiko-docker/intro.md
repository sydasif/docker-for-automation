# Containerizing Network Automation with Docker and Netmiko

**Introduction:**

Docker simplifies the deployment and isolation of applications using containers. In this blog series, we explore Docker from both operational and development perspectives. In our first blog, we covered the Ops Perspective, where we installed Docker, downloaded an image, started a container, ran commands, and destroyed it. Now, let's delve into the Dev Perspective by building a custom container for network automation using Python and Netmiko.

**Setting up Your Dev Environment:**

To begin, create a directory with the following structure:

```bash
$ tree
.
├── app.py
└── Dockerfile
```

In this structure, `app.py` contains the Netmiko script we'll include in our container image, and `Dockerfile` contains the build instructions.

**Writing the Netmiko Script (app.py):**

```python
from netmiko import ConnectHandler

# Create a dictionary for a particular Device
R1 = {'device_type': 'cisco_ios',
      'ip': '10.10.10.9',
      'username': 'admin',
      'password': 'cisco'}

net_connect = ConnectHandler(**R1)

# Sending a command to the Device
output = net_connect.send_command("show ip int br")
# Print the output
print(output)
```

**Creating the Dockerfile:**

```Dockerfile
# Use an official Python runtime as a parent image
FROM python:slim-bullseye

# Metadata information
LABEL name="Python Automation"
LABEL maintainer="sydasif78@gmail.com"

# Update setup
RUN apt-get update 

# Install pip and netmiko
RUN pip install --upgrade pip \
&& pip install netmiko 

# Set the working directory to /home
WORKDIR /home

# Copy the app.py into the container home directory
COPY app.py /home
```

Let’s walk through the Dockerfile:

1. The “FROM” statement load / pull the container we are going to extend.
2. The “LABEL” statement sets your information (optional).
3. The “RUN” statement executes a command in the container.
4. The “WORKDIR” statement, the default directory.
5. The “COPY” statement copies the app.py on the host system to the specified directory in the Docker container.

> If there is two “WORKDIR” statement in the script, the default directory you start in will be the last WORKDIR in your Dockerfile.

**Building the Docker Image:**

Use the following command to build the custom image from your Dockerfile:

```bash
$ docker build -f ./Dockerfile -t automation .
```

- The `-t automation` sets the image name.
- The dot at the end specifies the build context.

When you run this command, Docker will first grab the Python base image from Docker Hub. This image is then started as a new container, in which the instructions from the Dockerfile are performed.

After these instructions, the Python container now holds the changes and will be saved as a new image, ready for use. When the build command finishes, you have both the Python image and your new custom image.

```shell
$ docker image ls

REPOSITORY   TAG             IMAGE ID       CREATED         SIZE
automation   latest          828154ade6ae   6 seconds ago   199MB
python       slim-bullseye   ba94a8d11761   3 days ago      125MB
```

**Running the Container:**

After building the image, you can run a container from it and test the app:

```bash
$ docker run --name netmiko -it automation bash
root@daf6ab851974:/home# ls
app.py

root@daf6ab851974:/home# cat app.py 
from netmiko import ConnectHandler

# Create a dictionary for a particular Device
R1 = {'device_type': 'cisco_ios',
      'ip': '10.10.10.9',
      'username': 'admin',
      'password': 'cisco'}

'''calling the ConnectHandler Library [**R1] means telling python 
to consider the contents of the dictionary as key-value pairs 
instead of single elements.'''
net_connect = ConnectHandler(**R1)

# Sending a command in to the Device
output = net_connect.send_command("show ip int br")
# Print out the send command
print(output)
root@daf6ab851974:/home#
```

This command connects you to a bash shell inside the container, allowing you to interact with the environment. To exit the container, simply type `exit`.

**Sharing Docker Images:**

To share your container image on Docker Hub, create a private repository, log in, tag the existing image with your repository information, and push it:

```bash
$ docker login

$ docker image tag automation sydasif78/python:latest

$ docker image push sydasif78/python:latest 
```

With these steps, you have a functional container ready for your network automation projects. In the next blog, we'll connect the container to a network in GNS3 to test its functionality.

**Conclusion:**

By containerizing your network automation tools, you avoid dependency issues and maintain a clean development environment. Docker empowers you to efficiently manage and share your network automation workflows. Stay tuned for our next blog where we take our containerized solution to the next level.
